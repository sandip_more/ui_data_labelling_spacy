import argparse
import re
import xml.etree.ElementTree as ET
import zipfile
import os
import sys

nsmap = {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}


def qn(tag):
    """
    Stands for 'qualified name', a utility function to turn a namespace
    prefixed tag name into a Clark-notation qualified tag name for lxml. For
    example, ``qn('p:cSld')`` returns ``'{http://schemas.../main}cSld'``.
    Source: https://github.com/python-openxml/python-docx/
    """
    prefix, tagroot = tag.split(':')
    uri = nsmap[prefix]
    return '{{{}}}{}'.format(uri, tagroot)


def mydocx2txt(docx):
    try:
        zipf = zipfile.ZipFile(docx)
    except zipfile.BadZipfile:
        return u""
    doc_xml = 'word/document.xml'
    xml=zipf.read(doc_xml)
    root=ET.fromstring(xml)
    body_element=None
    text=u""
    for e in root:
        if e.tag==qn("w:body"):
            # print(e)
            # print(f"{e.tag}")
            body_element=e
            break
    for e in body_element:
        # print(e)
        # print(f"{e.tag} {e.attrib}")
        if e.tag==qn("w:p"):
            #only text u canexpect
            # print(f"{e.tag} {e.attrib}")
            p_text=u"\n"
            for p in e.iter():
                if(p.tag==qn("w:t")):
                    # print(f"{p.tag} {p.text}")
                    p_text=p_text+p.text
            text=text+p_text
        if e.tag == qn('w:tbl'):
            # print(child)
            t_text=u"<table_start>"
            n_rows=0
            n_cols=0
            for tbl_child in e.iter():
                if(tbl_child.tag == qn('w:tr')):
                    n_rows=n_rows+1
                    r_text=u""
                    # print(f"{tbl_child.tag}    {tbl_child.text} ")
                    n_cols=0
                    for col in tbl_child.iter():
                        if(col.tag==qn("w:tc")):
                            # print(f"{col.tag}    {col.text} ")
                            n_cols=n_cols+1
                            c_text=u""
                            for col_e in col.iter():
                                if(col_e.tag==qn("w:t")):
                                    # print(f"{repr(col_e.text)} ")
                                    c_text=c_text+u" "+col_e.text
                            c_text=c_text.lstrip()
                            # print(repr(c_text))
                            r_text=r_text+"\n\n"+c_text
                    # print(repr(r_text))
                    t_text=t_text+r_text
            # tbl_dimensions=dict({"NumberOfColumns":n_cols,"NumberOfRows":n_rows})
            # print(str(tbl_dimensions))
            t_text=t_text+u"\n\n<NumberOfColumns="+str(n_cols)+u">"+u"<NumberOfRows="+str(n_rows)+u">"+u"<table_end>"
            # print(repr(t_text))
            # t_text=t_text+str(tbl_dimensions)+u"<table_end>"
            text=text+t_text
    # print(repr(text))        
    zipf.close()
    text=text.replace("\n"," # ")
    text=text.replace("\t"," * ")
    return text
def to_matrix(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]
def isNotBlank (myString):
    return bool(myString and myString.strip())    
if __name__=="__main__":
    os.chdir("C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle")
    docx="Amulya Komatineni.docx"
    txt=mydocx2txt(docx)
    print(repr(txt))