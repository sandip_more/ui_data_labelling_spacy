# -*- coding: utf-8 -*-



import json
import json
# from pathlib import Path
from  yourapp.mydocx2txt import mydocx2txt
# from  mydocx2txt import mydocx2txt
import re
import unicodedata
from pathlib import Path
limit=50
# from pprint import pprint
# Python program to convert a list to string 
# Function to convert 
def listToString(s): 
    
    # initialize an empty string 
    str1 = "" 
    le=s.pop()
    # traverse in the string 
    for ele in s: 
        str1 = str1+str(ele)+str('|') 
    
    # return string 
    return str1+str(le) 
        
def getRegularExpression():
    # with open(os.path.join(__location__, 'data.json')) as f:
    
    p = Path(__file__).with_name('data.json')
    with p.open('r') as f:
    # with open('data.json') as f:
        keywords_dict = json.load(f)
    all_kw=[]
    for k in keywords_dict:
        all_kw.extend(sorted(keywords_dict[k],reverse=True))
    all_kw=sorted(all_kw,reverse=True)
    # Driver code	 
    s = listToString(all_kw) 
    return s

# example_02.py

def isBlank (myString):
    return not (myString and myString.strip())

def isNotBlank (myString):
    return bool(myString and myString.strip())


def getSplitedText(found_keywords,clean_text):
    n=len(found_keywords)+1
    keyword_text=[]
    for k in range(n):
        if(k==0):
            keyword_text.append(tuple(('Head',clean_text[0:found_keywords[0][0]],0,found_keywords[0][0])))
            #print(keyword_text)
        elif(k==n-1):
            keyword_text.append(tuple((found_keywords[k-1][2],clean_text[found_keywords[k-1][1]:len(clean_text)],found_keywords[k-1][1],len(clean_text))))
        else:
            keyword_text.append(tuple((found_keywords[k-1][2],clean_text[found_keywords[k-1][1]:found_keywords[k][0]],found_keywords[k-1][1],found_keywords[k][0])))
    return (keyword_text)

def process(text,keywords):  
    
    # text = mydocx2txt(src_path)
    # print(repr(text))
    count=0
    if text:
        # clean_text = unicodedata.normalize("NFKD",text)
        found_keywords=[]
        match=re.finditer(keywords, text,re.IGNORECASE)
        # match=re.finditer(keywords, clean_text)
        for m in match:
            if isNotBlank(m.group(0)):
                found_keywords.append((m.start(), m.end(), m.group(0)))
                count=count+1
         #if no keyword found ignore or find only those fields which are possible to extract from whole document i.e clean_text
        # print(found_keywords)
        if(count==0):
                return None
        if(count>0):
                keyword_text=getSplitedText(found_keywords,text)
                # print(keyword_text)
                return keyword_text







def getSectionizedData(text,reg_exp,keywords_dict):
    if(text==u""):
        return None
    d=dict()
    d["PROFILE_SUMMARY"]=None
    d["SKILLS"]=None
    d["EXPERIENCE"]=None
    d["EDUCATION"]=None
    d["OTHER"]=None
    d["HEAD"]=None
    d["CERTIFICATIONS"]=None
    d["GARBAGE"]=None
    try:

        temp=process(text,reg_exp)
        if(temp==None):
            return None
        # print(f"{type(temp)}  {len(temp)} {type(temp[0])}  {len(temp[0])} ")
        # print(f"{(temp[0])} ")
        for x in temp:
            # print(x[0])

            if(x[0] in keywords_dict["PROFILE_SUMMARY"]):
                t=dict()
                t['keyword_found']=x[0]
                t['text_found']=x[1]
                t['start_pos']=x[2]
                t['end_pos']=x[3]
                d["PROFILE_SUMMARY"]=t
                
            elif(x[0] in keywords_dict["SKILLS"]):
                t=dict()
                t['keyword_found']=x[0]
                t['text_found']=x[1]
                t['start_pos']=x[2]
                t['end_pos']=x[3]
                d["SKILLS"]=t
            elif(x[0] in keywords_dict["EXPERIENCE"]):
                t=dict()
                t['keyword_found']=x[0]
                t['text_found']=x[1]
                t['start_pos']=x[2]
                t['end_pos']=x[3]
                d["EXPERIENCE"]=t
            elif(x[0] in keywords_dict["EDUCATION"]):
                t=dict()
                t['keyword_found']=x[0]
                t['text_found']=x[1]
                t['start_pos']=x[2]
                t['end_pos']=x[3]
                d["EDUCATION"]=t
            elif(x[0] in keywords_dict["OTHER"]):
                t=dict()
                t['keyword_found']=x[0]
                t['text_found']=x[1]
                t['start_pos']=x[2]
                t['end_pos']=x[3]
                d["OTHER"]=t
            elif(x[0] in ["Head"]):
                t=dict()
                t['keyword_found']=x[0]
                t['text_found']=x[1]
                t['start_pos']=x[2]
                t['end_pos']=x[3]
                d["HEAD"]=t

            elif(x[0] in keywords_dict["CERTIFICATIONS"]):    
                t=dict()
                t['keyword_found']=x[0]
                t['text_found']=x[1]
                t['start_pos']=x[2]
                t['end_pos']=x[3]
                d["CERTIFICATIONS"]=t

            else:
                t=dict()
                t['keyword_found']=x[0]
                t['text_found']=x[1]
                t['start_pos']=x[2]
                t['end_pos']=x[3]
                d["GARBAGE"]=t
        
        return d
    
    except:
        print("Can't process file")
        return None

def getkeywords_dict():
    p = Path(__file__).with_name('data.json')
    with p.open('r') as f:
    
    # with open('data.json') as f:
        keywords_dict = json.load(f)
        return keywords_dict
def contaisnColon(text_list):
    c=0
    for t in text_list:
        if(t.__contains__(":")):
            # return True
            c=c+1
    if(c==len(text_list)):
        return True
    return False
def tableisPresent(text,start_pos):
    #check if table is present in text[start_pos:start_pos+limit]
    search_text=text[start_pos:]
    import re 
    # re.findall('<.*?>', s)
    reg_exp=r"<table_start>[\s\S]*?<table_end>"
    p=re.compile(reg_exp)
    match=p.finditer(search_text)
    tables_text=[]
    
    for m in match:
        if isNotBlank(m.group(0)):
            tables_text.append((m.start(), m.end(), m.group(0)))
    if(len(tables_text)>0):
        if(tables_text[0][0])<=limit:
            # print(f"table start: {tables_text[0][0]+start_pos} table end:{tables_text[0][1]+start_pos} skills_text_start_pos:{start_pos} ")
            p_t=re.compile(r"\n\n.+")
            p_d_rows=re.compile(r"<NumberOfRows=\d+>")
            p_d_cols=re.compile(r"<NumberOfColumns=\d+>") 
            # print(repr(tables_text[0][2]))
            match=p_t.findall(tables_text[0][2]) 
            # print(len(match))  
            # print((match))  
            
            
            
            dim_t=match.pop()
            
            
            
            tt_r=p_d_rows.search(dim_t).group(0)
            n_rows=re.search(r"\d+",tt_r).group(0)
            # print(tt_r)
            # print(n_rows)
            tt_c=p_d_cols.search(dim_t).group(0)
            n_cols=re.search(r"\d+",tt_c).group(0)
            # print(tt_c)
            # print(n_cols)
            n_cols=int(n_cols)
            if(n_cols==2 or n_cols==3):
                # print(match)
                
                keys=[]
                values=[]
                for i in range(len(match)):
                    if(i%2==0):
                        keys.append(match[i])
                        #match[i] is key
                    else:
                        values.append(match[i])
                        #match[i] is value
                if(len(keys)!=len(values)):
                    return False,None 
                if(contaisnColon(keys) and contaisnColon(values)):
                    # pass
                    #get keys data and values data into tab_dict 
                    d=dict()
                    for i in keys:
                        t=i.split(':')
                        if(len(t)==2):
                            k=" ".join(t[0].split())
                            v=" ".join(t[1].split())
                            d[k]=v.split(',')
                    for i in values:
                        t=i.split(':')
                        if(len(t)==2):
                            k=" ".join(t[0].split())
                            v=" ".join(t[1].split())
                            d[k]=v.split(',')
                    return True,d
                tab_dict=dict()
                for i in range(len(keys)):
                    # tab_dict[keys[i]]=values[i]
                    tab_dict[" ".join(keys[i].split())]=" ".join(values[i].split()).split(",")
                return True,tab_dict
    
    return False,None
unwanted_keys=['Client','Environment','Description','Responsibilities','http','https']   
def getSkillsfromText(txt):
    #assuming data is in format \nk:v
    match_colon=re.findall(r"\n.+[:].+?\n",txt)
    if(len(match_colon)>0):
        skills=dict()
        for m in match_colon:
            # print(type(m))
            #separate string m into two sections based on :
            r=m.split(":")
            if(len(r)==2):
                k=" ".join(r[0].split())
                v=" ".join(r[1].split())
                if(k in unwanted_keys):
                    continue
                skills[k]=v.split(",")
        return skills
    match_lines=re.findall(r"\n.+?\n",txt)
    if(len(match_lines)>0):
        skills=dict()
        line=0
        for m in match_lines:
            skills[line]=" ".join(m.split())
            line=line+1
        return skills
def extractSkills(text):
    # text = mydocx2txt(file)
    reg_exp=getRegularExpression()
    keywords_dict=getkeywords_dict()
    ParseDict=getSectionizedData(text,reg_exp,keywords_dict)

    if(ParseDict):
        if(ParseDict['SKILLS']):
            # print(repr(ParseDict['SKILLS']))
            # print(ParseDict['SKILLS']['start_pos'])
            # print(ParseDict['SKILLS']['end_pos'])
            # # print(repr(text[ParseDict['SKILLS']['start_pos']:ParseDict['SKILLS']['end_pos']]))
            flag,value=tableisPresent(text,ParseDict['SKILLS']['start_pos'])
    #         skills_text=ParseDict['SKILLS']
            if(flag):
                # if table present in skills_text process accordingly
                # print("Table Present")
                # print(repr(ParseDict['SKILLS']))
                # print(repr(value))
                return value
                # pass
            else:
                # print("Table Not Present")
                s=getSkillsfromText(ParseDict['SKILLS']['text_found']) 
                # print(repr(ParseDict['SKILLS']))
                # print(repr(s))  
                return s
                # pass
    return dict()
def containsDigit(s):
    flag=False
    for ch in s:
        if(ch.isnumeric()):
            flag=True
            break
    return flag
def isValidName(i):
    # #if lst[i] is not number or email id(not containing @) or URL(not containing .)
    flag=True 
    if(i.isnumeric() or i.__contains__("@")  or i.__contains__("<") or i.__contains__(".") or containsDigit(i)):
        flag=False
    return flag
def getNameSummaryfromText(txt):
    # print(repr(txt))
    # d=dict({"Name":None,})
    #get name assuming to be first line
    match=re.search(r"\n.+\n",txt)
    if(match):
        name=" ".join(match.group(0).split())
        name=name.replace("Name","").replace(":",'').replace("-",'')
        lst=name.split()
        lst=[e for e in lst if(isValidName(e))]
            
            
        if(len(lst)>3 or len(lst)==0):
            return None
        else:
            return " ".join(lst) 
        # print(f"Name Found:{repr(name)}")  
    

def getEmailPhonefromText(txt):
  
    #find all email ids
    # lst_emails = re.findall('\S+@\S+', txt)

    # print(lst_emails)
    #find all phone numbers
    # lst_phones = re.findall('\S+@\S+', txt)
    # print(lst_emails)
    regex_number = re.compile(r'[:;\+\(]?[1-9][0-9 .\-\(\)]{8,}[0-9]')
    regex_email = re.compile(r'[:,A-Za-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[A-Za-z]+')
    PhoneNumber = regex_number.findall(txt)
    Email = regex_email.findall(txt)
    # print(PhoneNumber,Email)
    return dict({"Email":Email,"PhoneNumber":PhoneNumber})


if __name__=="__main__":

 import sys
 sys.stdout.reconfigure(encoding='utf-8')
#  sys.stdout.errors = 'replace'
 import os
 # os.chdir("C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle")
 # files=glob.glob('*.docx')
 # print(len(files))
 os.chdir("C:\\Users\\sandip more\\Desktop\\ResumeParsing\\RP_api\\New folder (2)")
 reg_exp=getRegularExpression()
 keywords_dict=getkeywords_dict()

 file="C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle\\Abiral_Pandey_Fullstack_Java.docx"
 file="C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle\\Bapuji Hadoop developer.docx"
 file="C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle\\Achyuth Resume_8.docx"


 
 import glob
 from pprint import pprint
 os.chdir("C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle")
 
 files=glob.glob('BA Kiran.docx')
 files=glob.glob('Yugesh_Resume.docx')
 files=glob.glob('Akhil.profile.docx')
 '''Amulya Komatineni.docx
Can't process file
Anil Krishna Mogalaturthi.docx'''
#  files=glob.glob('Amulya Komatineni.docx') 
 files=glob.glob('Anil Krishna Mogalaturthi.docx')
 files=glob.glob('Brahma-Resume (BA).docx')
#  files=glob.glob('Ami Jape.docx')

#  files=glob.glob('Resume Vishal PM - MSIS, PMP-PMI.docx')
 files=glob.glob('*.docx')
 for file in files:
    print(file)
    text = mydocx2txt(file)
    name=getNameSummaryfromText(text)
    if(name):
        print(name.split())
    EmailPhone=getEmailPhonefromText(text) 
    print(EmailPhone)        

    ParseDict=getSectionizedData(text,reg_exp,keywords_dict)

    if(ParseDict):
        if(ParseDict['SKILLS']):
            # print(repr(ParseDict['SKILLS']))
            # print(ParseDict['SKILLS']['start_pos'])
            # print(ParseDict['SKILLS']['end_pos'])
            # # print(repr(text[ParseDict['SKILLS']['start_pos']:ParseDict['SKILLS']['end_pos']]))
            flag,value=tableisPresent(text,ParseDict['SKILLS']['start_pos'])
    #         skills_text=ParseDict['SKILLS']
            if(flag):
                # if table present in skills_text process accordingly
                # print("Table Present")
                # print(repr(ParseDict['SKILLS']))
                # print(repr(value))
                pass
            else:
                # print("Table Not Present")
                s=getSkillsfromText(ParseDict['SKILLS']['text_found']) 
                # print(repr(ParseDict['SKILLS']))
                # print(repr(s))  
                # pass
        if(ParseDict['HEAD']):
            # print(repr(ParseDict['HEAD']))
            # name=getNameSummaryfromText(ParseDict['HEAD']['text_found'])
            # if(name):
            #     print(name.split())
            # EmailPhone=getEmailPhonefromText(ParseDict['HEAD']['text_found'])
            pass

    print("\n\n")