# -*- coding: utf-8 -*-



import json
import json
# from pathlib import Path
import docx2txt
import re
import unicodedata

# from pprint import pprint
# Python program to convert a list to string 
# Function to convert 
def listToString(s): 
    
    # initialize an empty string 
    str1 = "" 
    le=s.pop()
    # traverse in the string 
    for ele in s: 
        str1 = str1+str(ele)+str('|') 
    
    # return string 
    return str1+str(le) 
        
def getRegularExpression():
    with open('data.json') as f:
        keywords_dict = json.load(f)
    all_kw=[]
    for k in keywords_dict:
        all_kw.extend(sorted(keywords_dict[k],reverse=True))
    all_kw=sorted(all_kw,reverse=True)
    # Driver code	 
    s = listToString(all_kw) 
    return s

# example_02.py

def isBlank (myString):
    return not (myString and myString.strip())

def isNotBlank (myString):
    return bool(myString and myString.strip())

def extract_text_from_docx(docx_path):
    txt = docx2txt.process(docx_path)
    if txt:
        return txt.replace('\t','')
    return None

def getSplitedText(found_keywords,clean_text):
    n=len(found_keywords)+1
    keyword_text=[]
    for k in range(n):
        if(k==0):
            keyword_text.append(tuple(('Head',clean_text[0:found_keywords[0][0]])))
            #print(keyword_text)
        elif(k==n-1):
            keyword_text.append(tuple((found_keywords[k-1][2],clean_text[found_keywords[k-1][1]:len(clean_text)])))
        else:
            keyword_text.append(tuple((found_keywords[k-1][2],clean_text[found_keywords[k-1][1]:found_keywords[k][0]])))
    return (keyword_text)

def process(src_path,keywords):  
    
    text = extract_text_from_docx(src_path)
    # print(repr(text))
    count=0
    if text:
        clean_text = unicodedata.normalize("NFKD",text)
        found_keywords=[]
        match=re.finditer(keywords, clean_text,re.IGNORECASE)
        # match=re.finditer(keywords, clean_text)
        for m in match:
            if isNotBlank(m.group(0)):
                found_keywords.append((m.start(), m.end(), m.group(0)))
                count=count+1
         #if no keyword found ignore or find only those fields which are possible to extract from whole document i.e clean_text
        print(found_keywords)
        if(count==0):
                return []
        if(count>0):
                keyword_text=getSplitedText(found_keywords,clean_text)
                return keyword_text







def getSectionizedData(file,reg_exp,keywords_dict):
    # print(file)
    PROFILE_SUMMARY=''
    SKILLS=''
    EXPERIENCE=''
    EDUCATION=''
    OTHER=''
    HEAD=''
    GARBAGE=''
    CERTIFICATIONS=''
    try:
        temp=process(file,reg_exp)
        for x in temp:
            # print(x[0])

            if(x[0] in keywords_dict["PROFILE_SUMMARY"]):
                PROFILE_SUMMARY +=x[1] 
            elif(x[0] in keywords_dict["SKILLS"]):
                SKILLS +=x[1] 
            elif(x[0] in keywords_dict["EXPERIENCE"]):
                EXPERIENCE +=x[1] 
            elif(x[0] in keywords_dict["EDUCATION"]):
                EDUCATION += x[1] 
            elif(x[0] in keywords_dict["OTHER"]):
                OTHER += x[1] 
            elif(x[0] in ["Head"]):
                HEAD += x[1] 
            elif(x[0] in keywords_dict["CERTIFICATIONS"]):    
                CERTIFICATIONS +=x[1] 
            else:
                GARBAGE +=x[1]
        d=dict()
        d["PROFILE_SUMMARY"]=PROFILE_SUMMARY
        d["SKILLS"]=SKILLS
        d["EXPERIENCE"]=EXPERIENCE
        d["EDUCATION"]=EDUCATION
        d["OTHER"]=OTHER
        d["HEAD"]=HEAD
        d["CERTIFICATIONS"]=CERTIFICATIONS
        d["GARBAGE"]=GARBAGE
        return d
    
    except:
        print("Can't process file")
        return None

def getkeywords_dict():
    with open('data.json') as f:
        keywords_dict = json.load(f)
        return keywords_dict


if __name__=="__main__":
 import os
 # os.chdir("C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle")
 # files=glob.glob('*.docx')
 # print(len(files))
 os.chdir("C:\\Users\\sandip more\\Desktop\\ResumeParsing\\RP_api\\New folder (2)")
 reg_exp=getRegularExpression()
 keywords_dict=getkeywords_dict()

 file="C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle\\Abiral_Pandey_Fullstack_Java.docx"
 file="C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle\\Bapuji Hadoop developer.docx"
 file="C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle\\Achyuth Resume_8.docx"


 
 import glob
 from pprint import pprint
 os.chdir("C:\\Users\\sandip more\\Desktop\\ResumeParsing\\flair\\Resumes_Kaggle")
 
 files=glob.glob('BA Kiran.docx')
 files=glob.glob('*.docx')
 for file in files:
    print(file)
    ParseDict=getSectionizedData(file,reg_exp,keywords_dict)
    if(ParseDict):
        if(ParseDict['EDUCATION']):
            print(repr(ParseDict['EDUCATION']))
            ed=ParseDict['EDUCATION']
 
    print("\n\n")