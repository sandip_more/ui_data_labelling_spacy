# from mlflow.utils.file_utils import ENCODING
import yourapp.b64totxt as b64totxt
# import b64totxt
import re
import yourapp.extractDateText as extractDateText
from yourapp.skills import extractSkills
import os
import json
from flask import Flask, render_template, request, jsonify



import spacy
 

model_uri ="C:\\Users\\sandip more\\Desktop\\ResumeParsing\\RP_api\\New folder\\mlruns\\4d50e88f9d444c0cab6d602de6ef8658\\artifacts\\model\\model.spacy"
nlp = spacy.load(model_uri) 

app=Flask(__name__,template_folder='templates')
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.docx', '.pdf']
app.config['UPLOAD_PATH'] = 'uploads'

def getbase64encodedString(txt):
    match=re.match(r"data:application/.+;base64,",txt)
    if(match):
        return txt[match.end():]
    return u""


def getResposeDataFields(file):
    with open(file,"r") as f:
        data = json.load(f)
        return data 
def getskillsList(x:dict)->list:
    l=list(x.values())
    L=[]
    for i in l:
        if(isinstance(i,list)):
            for j in i:
                L.append(j)
        else:
            L.append(i)
    return L



@app.route('/', methods=['POST'])
def upload_files():

    
    data=request.get_data()

    # start = time.process_time()
    rec_data=json.loads(data)

    filename = rec_data["name"]
    filedata=rec_data['data']
    # start = time.process_time()
    filedata=getbase64encodedString(filedata)



    
    
    # filename = secure_filename(rec_data["name"])
    filename = rec_data["name"]
    print(filename)
    if filename != '':
        file_ext = os.path.splitext(filename)[1]
       
        if file_ext not in app.config['UPLOAD_EXTENSIONS']:
            return render_template('error.html')
        
        b64E=bytes(filedata,encoding='utf-8')
        txt=b64totxt.bytesBase64toText(b64E,file_ext)

        dt=extractDateText.getDatestext(txt)
        ExtractedDataExpereince=[]
        for d in dt:
            doc = nlp(d['text'])
            dd=dict()
            dd['period']=d['period']
            dd['Entities']=[(ent.text, ent.label_) for ent in doc.ents]
            ExtractedDataExpereince.append(dd)
        
        #extract skills
        skills_dict=extractSkills(txt)

        print(f"{repr(skills_dict)} {type(skills_dict)}")
        Skills=None
        if(skills_dict):
            print("Skills found")
            Skills=getskillsList(skills_dict)
     


        #create response
        ParsedResume=getResposeDataFields('output.json')
        ParsedResume["Skills"]=Skills 
        ParsedResume["FirstName"]=None #need to be changed
        ParsedResume["LastName"]=None #need to be changed
        ParsedResume["MiddleName"]=None #need to be changed
        ParsedResume["Mobile"]=None #need to be changed
        ParsedResume["PrimaryEmail"]=None #need to be changed
        ParsedResume["ProfileSummary"]=None

        ParsedResume["EmployementHistory"]=ExtractedDataExpereince
        json_reponse=dict()
        json_reponse["Status"]="200"
        json_reponse["Data"]={"ParsedResume":ParsedResume}
        json_reponse["Message"]="Success"


    # return jsonify(ExtractedDataExpereince)
    return jsonify(json_reponse)
    # return jsonify({})
    # # return redirect(url_for('index'))



@app.route('/')
def hello_world():
    return 'Hello Flask under Apache!'

if __name__ == "__main__":
    app.run()